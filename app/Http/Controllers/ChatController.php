<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Events\TestEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;

class ChatController extends Controller
{
	public function index()
	{
		return view('chat');
	}

	public function users()
    {
        return response()->json(User::where('id','!=', Auth::user()->id)->get());
    }

	public function send(Request $request)
	{
		$data = [
			'message' => $request->input('message'),
		];
		$messages = new Message();
        $messages->sender_id = $request->user()->id;
        $messages->receiver_id = $request->get('receiver_id');
        $messages->message = $request->get('message');
        $messages->channel = $request->get('channel');
        $messages->save();

        $receiver = User::where('id', $request->get('receiver_id'))->get();

        broadcast(new TestEvent($data));
        dispatch(new SendEmailJob($receiver, $messages));
//        SendEmailJob::dispatch($receiver)
    }

	public function chatWith($id)
    {
        $messages = Message::where(function($query) use ($id) {
            $query->where('sender_id', auth()->id());
            $query->where('channel', 'news-action');
            $query->where('receiver_id', $id);
        })->orWhere(function($query) use ($id) {
            $query->where('sender_id', $id);
            $query->where('channel', 'news-action');
            $query->where('receiver_id', auth()->id());
        })->get();

        return response()->json($messages);
    }
}
