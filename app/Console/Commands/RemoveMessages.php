<?php

namespace App\Console\Commands;

use App\Message;
use Illuminate\Console\Command;

class RemoveMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Message::where('created_at', '<',
            now()->subDay()->format('Y-m-d H:i:s')
        )->delete();
    }
}
