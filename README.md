# Laravel Vue.js Socket.io Chat (+ Redis)
Simple Laravel Vue.js Socket.io Chat (+ Redis)


### Server requirements:
- Node.js
- Redis
- Composer

### How to use:
1. Clone this repository
2. Run `composer install`
3. Run `npm install` (Node.js should be installed)
4. Make sure you use `BROADCAST_DRIVER=redis` in .env file
5. `node server.js`
6. `php artisan serve`

